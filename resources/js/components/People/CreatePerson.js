import React, { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import { 
  CssBaseline,
  Grid,
  Avatar,
  Box,
  Typography,
  TextField,
  Button,
  Paper,
  IconButton 
} from '@material-ui/core';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import CancelIcon from '@material-ui/icons/Cancel';
import SaveIcon from '@material-ui/icons/Save';
import Alert from '@material-ui/lab/Alert';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  image: {
    backgroundImage: 'url(https://images.saatchiart.com/saatchi/746251/art/3317772/2387659-CANGRIDD-7.jpg)',
    backgroundRepeat: 'no-repeat',
    backgroundColor:
      theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  color: {
    background: '#9c27b0',
  },
  fontErrorColor: {
    color: 'red',
  },
}));

export default function CreatePerson(props) {
  const classes = useStyles();
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [errorsName, setErrorsName] = useState([]);
  const [errorsEmail, setErrorsEmail] = useState([]);

  const handleSubmit = (e) => {
    e.preventDefault();    
    axios.post('http://127.0.0.1:8000/api/pessoas', { name, email })
    .then(res => {
      props.history.push({
        pathname: '/pessoas',
        state: {
          successMsg: res.data,
        }
      });
    }).catch(error => {
        setErrorsName(error.response.data.errors.name);
        setErrorsEmail(error.response.data.errors.email);
    });
  };

  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />      
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Avatar className={classes.avatar, classes.color}>
            <PersonAddIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Cadastrar Pessoa
          </Typography>
          <form className={classes.form} onSubmit={handleSubmit}>
            <TextField
              variant="outlined"
              margin="normal"            
              fullWidth
              name="name"
              label="Nome"
              type="text"
              id="name"
              autoFocus
              onChange={e => setName(e.target.value)}
            />  
            {errorsName && errorsName.length > 0 &&
              errorsName.map((error, i) => (
                <small key={i} className={classes.fontErrorColor}>{error}</small>
              ))
            }
            <TextField
              variant="outlined"
              margin="normal"
              fullWidth
              id="email"
              label="E-mail"
              name="email"
              type="email"              
              onChange={e => setEmail(e.target.value)}
            />        
            {errorsEmail && errorsEmail.length > 0 &&
              errorsEmail.map((error, i) => (
                <small key={i} className={classes.fontErrorColor}>{error}</small>
              ))
            }           
            <Box display="flex" justifyContent="center">
              <Box p={2} >
                <Button
                  variant="contained"
                  color="default"
                  className={classes.button}
                  startIcon={<CancelIcon />}
                  onClick={() => props.history.push('/pessoas')}
                >
                  Cancelar
                </Button>
              </Box>
              <Box py={2}>
                <Button
                  variant="contained"
                  color="primary"
                  className={classes.button}
                  startIcon={<SaveIcon />}
                  type="submit"
                >
                  Salvar
                </Button> 
              </Box>        
            </Box>        
          </form>
        </div>
      </Grid>
      <Grid item xs={false} sm={4} md={7} className={classes.image} />      
    </Grid>
  );
}