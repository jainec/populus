import React, { useState, useEffect } from 'react';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import ListPeople from './ListPeople';
import NewPerson from './NewPerson';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';

export default function PeopleContainer({ history }) {  
    const [open, setOpen] = useState(false);
    const [successMessage, setSuccessMessage] = useState('');

    useEffect(() => {
        if(history.location.state) {
            setSuccessMessage(history.location.state.successMsg.message);
            window.history.replaceState(null, null, "/pessoas");
            setOpen(true);
        }
    }, [])

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
        setSuccessMessage('');
        setOpen(false);        
    };

    const getMessage = (successMsg) => {
        setSuccessMessage(successMsg);
        setOpen(true);
    };

    return (
        <div>
            {successMessage && 
                <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
                    <Alert onClose={handleClose} severity="success">
                        {successMessage}
                    </Alert>
                </Snackbar> 
            }                            
           <div style={{ width: '100%' }}>
                <Box display="flex">
                    <Box py={1} my={2} ml={1} flexGrow={1} >
                        <Typography component="h1" variant="h6">
                            Todas as pessoas
                        </Typography>   
                    </Box>
                    <Box py={1} my={1}>
                        <NewPerson />
                    </Box>    
                </Box>
            </div>       
            <ListPeople removePersonMsg={getMessage}/>
        </div>
    )
}

