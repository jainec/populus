import React from 'react';
import axios from 'axios';
import { 
  makeStyles,
  Modal,
  Backdrop,
  Fade,
  Button,
  IconButton,
  Box 
} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import CancelIcon from '@material-ui/icons/Cancel';


const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

export default function DeletePerson(props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);   
  const person_id = props.personID; 

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleRemovePerson = (e) => {    
    axios.delete(`http://127.0.0.1:8000/api/pessoas/${person_id}`)
    .then(res => {
      console.log(res.data);   
      props.removePerson(props.index, res.data.message);
      //window.location = "/pessoas";
    }).catch(error => {
      console.log(error)
    });
  };

  return (
    <div>
        <IconButton aria-label="delete" color="secondary" onClick={handleOpen}>
          <DeleteIcon />
        </IconButton> 
        <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          className={classes.modal}
          open={open}
          onClose={handleClose}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
          timeout: 500,
          }}
        >
          <Fade in={open}>
          <div className={classes.paper}>
            <h2 id="transition-modal-title">Deletar Pessoa</h2>
            <p id="transition-modal-description">Tem certeza que deseja deletar a pessoa {props.personName}?</p>                             
            <Box
              display="flex"
              justifyContent="center"
              alignItems="flex-end"               
              bgcolor="background.paper"
            >
              <Box p={1}>
                <Button
                  variant="contained"
                  color="default"
                  className={classes.button}
                  startIcon={<CancelIcon />}
                  onClick={handleClose}                
                >
                  Cancelar
                </Button>
              </Box>
              <Box p={1}>
                <Button
                  variant="contained"
                  color="secondary"
                  className={classes.button}
                  startIcon={<DeleteIcon />}
                  onClick={handleRemovePerson}
                >
                  Deletar
                </Button>
              </Box>              
            </Box>
          </div>
          </Fade>
        </Modal>
    </div>
  );
}
