import React, { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import { 
  CssBaseline,
  Grid,
  Avatar,
  Box,
  Typography,
  TextField,
  Button,
  Paper,
  IconButton 
} from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import CancelIcon from '@material-ui/icons/Cancel';
import SaveIcon from '@material-ui/icons/Save';
import Alert from '@material-ui/lab/Alert';

const useStyles = makeStyles((theme) => ({
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  color: {
    background: '#9c27b0',
  },
  fontErrorColor: {
    color: 'red',
  },
  errorInput: {
    borderColor: 'red',
  }
}));

export default function EditPerson(props) {
  const classes = useStyles();
  const {person_id} = useParams();

  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  
  const [errorsName, setErrorsName] = useState([]);
  const [errorsEmail, setErrorsEmail] = useState([]);

  const [showSuccess, setShowSuccess] = useState(false);
  const [showError, setShowError] = useState(false);

  const handleSubmit = (e) => {
    e.preventDefault();    
    axios.put(`http://127.0.0.1:8000/api/pessoas/${person_id}`, { name, email })
      .then(res => {
        setShowSuccess(true);
        setShowError(false);
        setErrorsName([]);
        setErrorsEmail([]);
      }).catch(error => {
        setErrorsName(error.response.data.errors.name);
        setErrorsEmail(error.response.data.errors.email);
        setShowError(true);
        setShowSuccess(false);
      });
  };

  useEffect(() => {
    axios.get(`http://127.0.0.1:8000/api/pessoas/${person_id}`)
    .then(res => {      
        setName(res.data.data.name);
        setEmail(res.data.data.email);
    })
  }, [])

  return (
    <div>
      {showError &&
        <Box display="flex" justifyContent="center" m={1} p={1} width="100%">
          <Box width="60%">
            <Alert severity="error">
              <b>Ops! </b>Não foi possível editar.
            </Alert>          
          </Box>
        </Box>  
      }  
      {showSuccess &&
        <Box display="flex" justifyContent="center" m={1} p={1} width="100%">
          <Box width="60%">
            <Alert severity="success">
              Pessoa editada com sucesso!
            </Alert>          
          </Box>
        </Box>   
      } 
      <Box display="flex" justifyContent="center" m={1} p={1}>       
        <CssBaseline />      
        <Grid item xs={12} sm={8} md={6} component={Paper} elevation={6} square>
          <div className={classes.paper}>
            <Avatar className={classes.avatar, classes.color}>
              <EditIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Editar Pessoa {name}
            </Typography>
            <form className={classes.form, classes.errorInput} onSubmit={handleSubmit}>         
              <TextField
                variant="outlined"
                margin="normal"              
                fullWidth
                name="name"
                label="Nome"
                type="text"
                id="name"        
                value={name ?? ''}              
                autoFocus
                onChange={e => setName(e.target.value)}                           
              />  
              {errorsName && errorsName.length > 0 &&
                errorsName.map((error, i) => (
                  <small key={i} className={classes.fontErrorColor}>{error}</small>
                ))
              }
              <TextField
                variant="outlined"
                margin="normal"
                fullWidth
                id="email"
                label="E-mail"
                name="email"
                value={email ?? ''}              
                type="email"
                onChange={e => setEmail(e.target.value)} 
                InputLabelProps={{
                  shrink: true,
                }}
              />       
              {errorsEmail && errorsEmail.length > 0 &&
                errorsEmail.map((error, i) => (
                  <small key={i} className={classes.fontErrorColor}>{error}</small>
                ))
              }
              <Box display="flex" justifyContent="center">
                <Box p={2} >
                  <Button
                    variant="contained"
                    color="default"
                    className={classes.button}
                    startIcon={<CancelIcon />}
                    onClick={() => props.history.push('/pessoas')}
                  >
                    Cancelar
                  </Button>
                </Box>
                <Box py={2}>
                  <Button
                    variant="contained"
                    color="primary"
                    className={classes.button}
                    startIcon={<SaveIcon />}
                    type="submit"
                  >
                    Salvar
                  </Button> 
                </Box>        
              </Box>                                         
            </form>
          </div>
        </Grid>
      </Box>
    </div>
  );
}