import React, { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import { 
  Table,
  TableBody,
  Box,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  IconButton 
} from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import DnsIcon from '@material-ui/icons/Dns';
import Pagination from '@material-ui/lab/Pagination';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';
import DeletePerson from './DeletePerson';
import BasicPagination from '../BasicPagination';
import PaginationItem from '@material-ui/lab/PaginationItem';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

export default function ListPeople(props) {
  const classes = useStyles();
  const [people, setPeople] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(0);  
  const pageNumbers = [];
  const { page } = useParams();

  for(let i = 1; i <= totalPages; i++) {
    pageNumbers.push(i);
  }

  useEffect(() => {
    const fetchPeople = () => {
      axios.get(`http://127.0.0.1:8000/api/pessoas?page=${currentPage}`)
      .then(res => {              
        setTotalPages(res.data.meta.last_page);
        setPeople( res.data.data );                         
      })
    }

    fetchPeople();
  }, [currentPage]);

  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  const removePerson = (index, successMsg) => {
    var array = [...people]; //separate copy of array
    array.splice(index, 1);    
    setPeople(array);  
    props.removePersonMsg(successMsg);
  };

  return (
    <Box>     
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell align="center">Nome</TableCell>
              <TableCell align="center">E-mail</TableCell>
              <TableCell align="center">Enderecos</TableCell>
              <TableCell align="center">Editar</TableCell>
              <TableCell align="center">Deletar</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {people.map((person, i) => (
              <TableRow key={person.id} index={i}>
                <TableCell  scope="row">
                  {person.id}
                </TableCell>
                <TableCell align="center">{person.name}</TableCell>              
                <TableCell align="center">{person.email}</TableCell>            
                <TableCell align="center">
                  <Link to={`/pessoa/${person.id}/enderecos`}>
                    <IconButton aria-label="address" color="default">
                      <DnsIcon />
                    </IconButton>
                  </Link>
                </TableCell>
                <TableCell align="center">
                  <Link to={`/pessoa/${person.id}/editar`}>
                    <IconButton aria-label="edit" color="primary">
                        <EditIcon />
                    </IconButton>
                  </Link>
                </TableCell>            
                <TableCell align="center">               
                  <DeletePerson personID={person.id} personName={person.name} index={i} removePerson={removePerson}/>
                </TableCell>              
              </TableRow>
            ))}
          </TableBody>
        </Table>    
      </TableContainer>    
      {totalPages > 0 &&
        <Box display="flex" justifyContent="center" p={2} mb={2}>
          <BasicPagination totalPages={totalPages} paginate={paginate}/>   
        </Box>
      }
    </Box>    
  );
}
