import React from 'react';
import Routes from '../router/Routes';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import ButtonAppBar from './ButtonAppBar';
import { Link, BrowserRouter } from 'react-router-dom';

export default function App() {
  return (
    <React.Fragment>
      <CssBaseline />
      <BrowserRouter>
        <ButtonAppBar />
          <Container >        
            <Routes />
          </Container>
      </BrowserRouter>
    </React.Fragment>
  );
}
