import React from 'react';
import {
  Avatar,
  Button,
  CssBaseline,
  Paper,
  Box,
  Grid,
  Typography
} from '@material-ui/core';
import { Link } from 'react-router-dom';
import GpsFixedIcon from '@material-ui/icons/GpsFixed';
import { makeStyles } from '@material-ui/core/styles';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <a color="inherit" href="https://github.com/jainec" target="_blank"> 
        Jaine Conceição
      </a>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  image: {
    backgroundImage: 'url(https://images.fineartamerica.com/images/artworkimages/mediumlarge/1/bright-watercolor-map-of-the-world-irina-sztukowski.jpg)',
    backgroundRepeat: 'no-repeat',
    backgroundColor:
      theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',    
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  color: {
    background: '#9c27b0',
  },
}));

export default function LandingPage() {
  const classes = useStyles();

  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={9} className={classes.image} />
      <Grid item xs={12} sm={8} md={3} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Avatar className={classes.avatar, classes.color}>
            <GpsFixedIcon />
          </Avatar>
          <Box mb={3}>
            <Typography component="h1" variant="h6">
                Bem-vindo(a) ao Populus
            </Typography>
          </Box>
          <Box mb={3}>
            <Typography variant="body1">
                Com o Populus você pode salvar dados de pesssoas e os seus endereços.
            </Typography>
          </Box>
          <Typography variant="body1">
            Para acessar o sistema use o botão abaixo.
          </Typography>
          <form className={classes.form} noValidate>
            <Link to="/pessoas">
              <Button
                  fullWidth
                  variant="contained"
                  color="primary"
                  className={classes.submit}
              >
              Visualizar pessoas
              </Button>           
            </Link>            
            <Box mt={5}>
              <Copyright />
            </Box>
          </form>
        </div>
      </Grid>
    </Grid>
  );
}