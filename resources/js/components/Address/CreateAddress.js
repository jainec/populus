import React, { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import { 
  CssBaseline,
  Grid,
  Avatar,
  Box,
  Typography,
  TextField,
  Button,
  Paper,
  FormControl,
  InputLabel,
  Select, 
} from '@material-ui/core';
import AddCircleIcon  from '@material-ui/icons/AddCircle';
import CancelIcon from '@material-ui/icons/Cancel';
import SaveIcon from '@material-ui/icons/Save';
import Alert from '@material-ui/lab/Alert';
import InputMask from 'react-input-mask';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  image: {
    backgroundImage: 'url(https://a.1stdibscdn.com/angela-wakefield-paintings-painting-of-chrysler-building-42nd-street-new-york-by-british-landscape-artist-for-sale/a_8173/a_36907021544792452747/New_York_Chrysler_Building_42ndStreet_master.JPG?width=1500)',
    backgroundRepeat: 'no-repeat',
    backgroundColor:
      theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  color: {
    background: '#9c27b0',
  },
  formControl: {
    marginTop: theme.spacing(2),
    marginRight: theme.spacing(7),
    minWidth: 120,
  },
  width75: {
    width: 500,   
    marginRight: theme.spacing(7),
  },
  width50: {
    width: 200,   
    marginRight: theme.spacing(4),
  },
  width300: {
    width: 230,       
  },
  width10: {
    width: 200,   
  },
  marginRight7: {
    marginRight: theme.spacing(7),
    width: 350,  
  },
  fontErrorColor: {
    color: 'red',
  },
}));

export default function CreateAddress(props) {
  const classes = useStyles();

  const {person_id} = useParams();

  const [street, setStreet] = useState('');
  const [street_number, setStreetNumber] = useState(0);
  const [complement, setComplement] = useState('');
  const [district, setDistrict] = useState('');
  const [zipcode, setZipcode] = useState('');
  const [city_id, setCityId] = useState(1100015);
  const [cities, setCities] = useState([]); 
  const [city_state, setCityState] = useState('');

  const [errorsStreet, setErrorsStreet] = useState([]);
  const [errorsStreetNumber, setErrorsStreetNumber] = useState([]);
  const [errorsDistrict, setErrorsDistrict] = useState([]);
  const [errorsZipcode, setErrorsZipcode] = useState([]);
  const [errorsCityId, setErrorsCityId] = useState([]);

  useEffect(() => {
    const fetchCities = () => {
      axios.get(`http://127.0.0.1:8000/api/cidades`)
      .then(res => {      
        setCities( res.data.data );
      })
    }

    fetchCities();
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    
    axios.post(`http://127.0.0.1:8000/api/pessoa/${person_id}/enderecos`, { 
      street, street_number, complement, district, zipcode, city_id 
    })
    .then(res => {
      props.history.push({
        pathname: `/pessoa/${person_id}/enderecos`,
        state: {
          successMsg: res.data,
        }
      });
    }).catch(error => {
      setErrorsStreet(error.response.data.errors.street);
      setErrorsStreetNumber(error.response.data.errors.street_number);
      setErrorsDistrict(error.response.data.errors.district);
      setErrorsZipcode(error.response.data.errors.zipcode);
      setErrorsCityId(error.response.data.errors.city_id);
      console.log(error.response);
    });
  };

  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />      
      <Grid item xs={12} sm={8} md={8} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Avatar className={classes.avatar, classes.color}>
            <AddCircleIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Cadastrar Endereço para 
          </Typography>
          <form className={classes.form} noValidate onSubmit={handleSubmit}>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  autoComplete="fname"
                  name="street"
                  variant="outlined"                  
                  fullWidth
                  id="street"
                  label="Nome da rua *"
                  autoFocus
                  onChange={e => setStreet(e.target.value)}
                />
                {errorsStreet && errorsStreet.length > 0 &&
                  errorsStreet.map((error, i) => (
                    <small key={i} className={classes.fontErrorColor}>{error}</small>
                  ))
                }
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  variant="outlined"
                  fullWidth
                  id="street_number"
                  label="Número *"
                  name="street_number"
                  autoComplete="lname"
                  onChange={e => setStreetNumber(e.target.value)}
                />
                {errorsStreetNumber && errorsStreetNumber.length > 0 &&
                  errorsStreetNumber.map((error, i) => (
                    <small key={i} className={classes.fontErrorColor}>{error}</small>
                  ))
                }
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"                  
                  fullWidth
                  id="complement"
                  label="Complemento"
                  name="complement"
                  onChange={e => setComplement(e.target.value)}
                />               
              </Grid>
              <Grid item xs={12} sm={4}>
                <TextField
                  variant="outlined"
                  fullWidth
                  name="district"
                  label="Bairro *"
                  id="district"
                  onChange={e => setDistrict(e.target.value)}
                />
                {errorsDistrict && errorsDistrict.length > 0 &&
                  errorsDistrict.map((error, i) => (
                    <small key={i} className={classes.fontErrorColor}>{error}</small>
                  ))
                }
              </Grid>  
              <Grid item xs={12} sm={4}>
              <InputMask
                mask="99999-999" 
                maskChar=" "                     
                onChange={e => setZipcode(e.target.value)}               
              >
                {() => <TextField 
                  variant="outlined"
                  fullWidth
                  name="zipcode"
                  label="Código postal *"
                  id="zipcode"                              
                />}
              </InputMask>
                {errorsZipcode && errorsZipcode.length > 0 &&
                  errorsZipcode.map((error, i) => (
                    <small key={i} className={classes.fontErrorColor}>{error}</small>
                  ))
                }
              </Grid>
              <Grid item xs={12} sm={4}>
                <FormControl variant="outlined">
                  <InputLabel htmlFor="outlined-age-native-simple">Cidade</InputLabel>
                  <Select
                    native
                    value={city_id}
                    label="Cidade *"
                    onChange={e => setCityId(e.target.value)}
                    inputProps={{
                        name: 'city_id',
                        id: 'outlined-age-native-simple',
                    }}
                  >
                  <option aria-label="None" value="" />
                  {cities.map(city => (
                    <option key={city.id} value={city.id}>{city.name} - {city.state}</option>                
                  ))}
                  </Select>
                </FormControl>  
                {errorsCityId && errorsCityId.length > 0 &&
                  errorsCityId.map((error, i) => (
                    <small key={i} className={classes.fontErrorColor}>{error}</small>
                  ))
                }
              </Grid>           
            </Grid>
            <Box display="flex" justifyContent="center">
              <Box p={2} >
                <Button
                  variant="contained"
                  color="default"
                  className={classes.button}
                  startIcon={<CancelIcon />}
                  onClick={() => props.history.push(`/pessoa/${person_id}/enderecos`)}
                >
                  Cancelar
                </Button>
              </Box>
              <Box py={2}>
                <Button
                  variant="contained"
                  color="primary"
                  className={classes.button}
                  startIcon={<SaveIcon />}
                  type="submit"
                >
                  Salvar
                </Button> 
              </Box>        
            </Box>            
          </form>         
        </div>
      </Grid>
      <Grid item xs={false} sm={4} md={4} className={classes.image} />
    </Grid>
  );
}