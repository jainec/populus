import React, { useEffect, useState } from 'react';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { useParams } from "react-router-dom";
import ListAddresses from './ListAddresses';
import NewAddress from './NewAddress';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';

export default function PeopleContainer({ history }) {
    const {person_id} = useParams();
    const [name, setName] = useState('');
    const [open, setOpen] = useState(false);
    const [successMessage, setSuccessMessage] = useState('');

    useEffect(() => {
        const fetchPerson = () => {
          axios.get(`http://127.0.0.1:8000/api/pessoas/${person_id}`)
          .then(res => {      
            setName(res.data.data.name);
          })
        }
    
        fetchPerson();
      }, []);

    useEffect(() => {
        if(history.location.state) {
            setSuccessMessage(history.location.state.successMsg.message);
            window.history.replaceState(null, null, `http://127.0.0.1:8000/api/pessoa/${person_id}/enderecos`);
            setOpen(true);
        }
    }, [])

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
        setSuccessMessage('');
        setOpen(false);        
    };

    const getMessage = (successMsg) => {
        setSuccessMessage(successMsg);
        setOpen(true);
    };

    return (
        <div>       
            {successMessage && 
                <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
                    <Alert onClose={handleClose} severity="success">
                        {successMessage}
                    </Alert>
                </Snackbar> 
            }       
            <Box display="flex">
                <Box py={1} my={2} ml={1} flexGrow={1} >
                    <Typography component="h1" variant="h6">
                        Todas os endereços de {name}
                    </Typography>   
                </Box>
                <Box py={1} my={1}>
                    <NewAddress person_id={person_id}/>
                </Box>    
            </Box>           
            <ListAddresses removeAddressMsg={getMessage}/>
        </div>
    )
}