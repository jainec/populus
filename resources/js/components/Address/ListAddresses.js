import React, { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import { 
  Table,
  TableBody,
  Box,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  IconButton 
} from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import DnsIcon from '@material-ui/icons/Dns';
import Pagination from '@material-ui/lab/Pagination';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';
import DeleteAddress from './DeleteAddress';
import BasicPagination from '../BasicPagination';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

export default function ListAddresses(props) {
  const classes = useStyles();
  const {person_id} = useParams();
  const [addresses, setAddresses] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(0);
  const addresses_length = addresses.length;

  useEffect(() => {
    const fetchAddress = () => {
      axios.get(`http://127.0.0.1:8000/api/pessoa/${person_id}/enderecos?page=${currentPage}`)
      .then(res => {      
        setAddresses( res.data.data );
      })
    }

    fetchAddress();
  }, [currentPage]);

  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  const removeAddress = (index, successMsg) => {
    var array = [...addresses]; //separate copy of array
    array.splice(index, 1);    
    setAddresses(array);  
    props.removeAddressMsg(successMsg);
  };

  return (
    <Box mb={5}>
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Rua</TableCell>
              <TableCell align="center">Bairro</TableCell>
              <TableCell align="center">Complemento</TableCell>
              <TableCell align="center">Codigo Postal</TableCell>
              <TableCell align="center">Cidade</TableCell>
              <TableCell align="center">Estado</TableCell>
              <TableCell align="center">Editar</TableCell>
              <TableCell align="center">Deletar</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>      
            {addresses.length > 0 &&
              addresses.map((address, i) => (
                <TableRow key={address.id}>
                  <TableCell  scope="row">
                    {address.street}, nº {address.street_number}
                  </TableCell>
                  <TableCell align="center">{address.district}</TableCell>
                  <TableCell align="center">{address.complement}</TableCell>
                  <TableCell align="center">{address.zipcode}</TableCell>   
                  <TableCell align="center">{address.city}</TableCell>   
                  <TableCell align="center">{address.state} - {address.state_abbr}</TableCell>                         
                  <TableCell align="center">
                    <Link to={`/pessoa/${person_id}/enderecos/${address.id}/editar`}>
                      <IconButton aria-label="edit" color="primary">
                          <EditIcon />
                      </IconButton>
                    </Link>
                  </TableCell>
                  <TableCell align="center">        
                    <DeleteAddress address_id={address.id} index={i} removeAddress={removeAddress}/>       
                  </TableCell>
                </TableRow>
              ))
            }          
          </TableBody>
        </Table>
        {addresses.length <= 0 &&
          <Box display="flex" justifyContent="center" p={2}>
            Essa pessoa não possui nenhum endereço cadastrado.
          </Box>
        }
      </TableContainer>
      {totalPages > 0 &&
        <Box display="flex" justifyContent="center" p={2} mb={2}>
          <BasicPagination totalPages={totalPages} paginate={paginate}/>        
        </Box>
      }
    </Box>
  );
}
