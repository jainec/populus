import React from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";

import PeopleContainer from '../components/People/PeopleContainer';
import EditPerson from '../components/People/EditPerson';
import CreatePerson from '../components/People/CreatePerson';

import AddressContainer from '../components/Address/AddressContainer';
import CreateAddress from '../components/Address/CreateAddress';
import EditAddress from '../components/Address/EditAddress';

import LandingPage from '../components/LandingPage';

function Routes() {
    return (
        <Switch>
            <Route path="/" exact component={LandingPage}/>            

            <Route path="/pessoas" exact component={PeopleContainer}/>                                    
            <Route path="/pessoa/:person_id/editar" exact component={EditPerson}/>     
            <Route path="/pessoas/cadastrar" exact component={CreatePerson}/>
                   
            <Route path="/pessoa/:person_id/enderecos" exact component={AddressContainer}/>
            <Route path="/pessoa/:person_id/enderecos/cadastrar" exact component={CreateAddress}/>
            <Route path="/pessoa/:person_id/enderecos/:address_id/editar" exact component={EditAddress}/>                        
        </Switch>
    )
}

export default Routes;