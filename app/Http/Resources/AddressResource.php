<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'person_id' => $this->person_id,
            'person_name' => $this->person->name,
            'street' => $this->street,
            'street_number' => $this->street_number,
            'complement' => $this->complement,
            'district' => $this->district,
            'zipcode' => $this->zipcode,
            'city_id' => $this->city_id,
            'city' => $this->city->name,
            'state' => $this->city->state->name,
            'state_abbr' => $this->city->state->abbr,
        ];
    }
}
