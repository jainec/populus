<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'city_id' => 'required',
            'street' => 'required|max:255',
            'street_number' => 'required',
            'district' => 'required|max:255',
            'zipcode' => 'required|max:30',
        ];
    }

    public function messages()
    {
        return [
            'city_id.required' => 'O campo cidade é obrigatório.',
            'street.required' => 'O campo rua é obrigatório.',
            'street.max' => 'O nome da rua deve ter no máximo 255 caracteres.',
            'street_number.required' => 'O campo número da casa é obrigatório.',
            'district.required' => 'O campo bairro é obrigatório.',
            'district.max' => 'O nome do bairro deve ter no máximo 255 caracteres.',
            'zipcode.required' => 'O campo código postal é obrigatório.',
            'zipcode.max' => 'O código postal deve ter no máximo 30 caracteres.',
        ];
    }
}
