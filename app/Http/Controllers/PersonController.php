<?php

namespace App\Http\Controllers;

use App\Http\Requests\PersonRequest;
use App\Http\Resources\PersonResource;
use App\Models\Person;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PersonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return PersonResource::collection(Person::orderBy('created_at', 'DESC')->paginate(10));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PersonRequest $request)
    {
        try {
            $person = Person::create($request->all());            
            return response()->json([
                'message' => 'Pessoa criada com sucesso!',
                'person' => new PersonResource($person),
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                'error' => 'Erro ao inserir no banco!'
            ], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function show($person)
    {
        try {            
            return new PersonResource(Person::findOrfail($person));
        } catch (Exception $e) {
            return response()->json([
                'error' => 'Pessoa nao encontrada!'
            ], 200);
        } 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function update(PersonRequest $request, $person)
    {
        try {            
            Person::findOrfail($person)->update($request->all());
            
            return response()->json([
                'message' => 'Pessoa atualizada com sucesso!',
                'person' => new PersonResource(Person::findOrfail($person)),
            ], 200);     
        } catch (Exception $e) {
            return response()->json([
                'error' => 'Pessoa nao encontrada!'
            ], 200);
        }  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function destroy($person)
    {        
        try {
            Person::findOrfail($person)->delete();
            return response()->json([
                'message' => 'Pessoa deletada com sucesso!'
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                'error' => 'Pessoa nao encontrada!'
            ], 200);
        }         
    }
}
