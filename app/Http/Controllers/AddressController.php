<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddressRequest;
use App\Http\Resources\AddressResource;
use App\Models\Address;
use App\Models\Person;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Person $person)
    {
        return AddressResource::collection($person->addresses()->paginate(10));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Person $person, AddressRequest $request)
    {        
        try {
            $address = $person->addresses()->create($request->all());
            return response()->json([
                'message' => 'Endereço criado com sucesso!',
                'address' => new AddressResource($address),
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                'error' => 'Erro ao inserir no banco!'
            ], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function show(Person $person, $address)
    {
        try {            
            $address = Address::findOrfail($address);
            return new AddressResource($address);
        } catch (Exception $e) {
            return response()->json([
                'error' => 'Endereço nao encontrado!'
            ], 200);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function update(Person $person, AddressRequest $request, $address)
    {
        try {
            Address::findOrfail($address)->update($request->all());            

            return response()->json([
                'message' => 'Endereço atualizado com sucesso!',
                'address' => new AddressResource(Address::findOrfail($address)),
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                'error' => 'Endereço nao encontrado!'
            ], 200);
        }
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function destroy(Person $person, $address)
    {
        try {
            Address::findOrfail($address)->delete();
            return response()->json([
                'message' => 'Endereço deletado com sucesso!'
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                'error' => 'Endereço nao encontrado!'
            ], 200);
        } 
    }
}
