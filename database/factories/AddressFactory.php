<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Address;
use App\Models\City;
use App\Models\Person;
use Faker\Generator as Faker;

$factory->define(Address::class, function (Faker $faker) {
    return [
        'person_id' => function () {
            return Person::all()->random();
        },
        'city_id' => function () {
            return City::all()->random();
        },
        'street' => $faker->address,
        'street_number' => $faker->randomDigit,
        'complement' => $faker->address,
        'district' => $faker->address,
        'zipcode' => $faker->randomDigit,
    ];
});
