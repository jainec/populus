<?php

use App\Models\Address;
use App\Models\Person;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            StateSeeder::class,
            CitySeeder::class,
        ]);

        factory(Person::class, 100)->create();
        factory(Address::class, 300)->create();
    }
}
