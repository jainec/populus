<?php

namespace Tests\Unit;

use App\Models\Address;
use App\Models\Person;
use CitySeeder;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use StateSeeder;

;
use Tests\TestCase;

class AddressTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testCreateAddress()
    {
        $this->seed(StateSeeder::class);

        $this->seed(CitySeeder::class);        

        $person = factory(Person::class)->create();

        $data = [
            'id' => 1,
            'street' => 'Rua dos Bobo',
            'street_number' => '000',
            'district' => 'Bairro Novo',
            'zipcode' => '123456-789',
            'city_id' => 1100015,
        ];

        $response = $this->postJson('/api/pessoa/'.$person->id.'/enderecos', $data);        
        
        $response
            ->assertStatus(200)
            ->assertJson([
                "message" => "Endereço criado com sucesso!",
                "address" => [
                    "id" => $data['id'],
                    "person_id" => $person->id,
                    "person_name" => $person->name,
                    "street" => $data['street'],
                    "street_number" => $data['street_number'],
                    "complement" => null,
                    "district" => $data['district'],
                    "zipcode" => $data['zipcode'],
                    "city" => "Alta Floresta d`Oeste",
                    "state" =>"Rondônia",
                    "state_abbr" => "RO"
                ],
            ]);
    }

    public function testDeleteAddress()
    {
        $person = factory(Person::class)->create();

        $address = factory(Address::class)->create([
            'city_id' => 1,
        ]);

        $response = $this->deleteJson('/api/pessoa/'.$person->id.'/enderecos/'.$address->id);
        
        $response
            ->assertStatus(200)
            ->assertJson([
                'message' => "Endereço deletado com sucesso!",
            ]);
    }

    public function testUpdateAddress()
    {
        $this->seed(StateSeeder::class);

        $this->seed(CitySeeder::class);        

        $person = factory(Person::class)->create();

        $address = factory(Address::class)->create([
            'city_id' => 1100015,
        ]);
        
        $address->street = 'Rua dos Bobos';

        $response = $this->putJson('/api/pessoa/'.$person->id.'/enderecos/'.$address->id, $address->toArray());        
            
        $response
            ->assertStatus(200)
            ->assertJson([
                "message" => "Endereço atualizado com sucesso!",
                "address" => [
                    "id" => $address->id,
                    "person_id" => $person->id,
                    "person_name" => $person->name,
                    "street" => $address->street,
                    "street_number" => $address->street_number,
                    "complement" => $address->complement,
                    "district" => $address->district,
                    "zipcode" => $address->zipcode,
                    "city" => "Alta Floresta d`Oeste",
                    "state" =>"Rondônia",
                    "state_abbr" => "RO"
                ],
            ]);
    }
}
