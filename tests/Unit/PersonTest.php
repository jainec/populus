<?php

namespace Tests\Unit;

use App\Models\Person;
use Illuminate\Foundation\Testing\DatabaseMigrations;;
use Tests\TestCase;

class PersonTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testCreatePerson()
    {
        $data = [
            'id' => 1,
            'name' => 'Jaine',
            'email' => 'jaine@ccs@gmail.com',
        ];

        $response = $this->postJson('/api/pessoas', $data);
        
        $response
            ->assertStatus(200)
            ->assertJson([
                "message" => "Pessoa criada com sucesso!",
                "person" => $data,
            ]);
    }

    public function testDeletePerson()
    {
        $person = factory(Person::class)->create();

        $response = $this->deleteJson('/api/pessoas/'.$person->id);

        $response
            ->assertStatus(200)
            ->assertJson([
                'message' => "Pessoa deletada com sucesso!",
            ]);
    }

    public function testUpdatePerson()
    {
        $person = factory(Person::class)->create();

        $person->name = 'Jaine Conceicao';

        $response = $this->putJson('/api/pessoas/'.$person->id, $person->toArray());

        $response
            ->assertStatus(200)
            ->assertJson([
                'message' => 'Pessoa atualizada com sucesso!',
                'person' => [
                    'id' => $person->id,
                    'name' => 'Jaine Conceicao',
                    'email' => $person->email,
                ]
            ]);
    }
}
